<?php

require('config.php');
require('twitteroauth/twitteroauth.php');

session_start();

// we can skip this
if (!empty($_SESSION['awesm_api_key'])) {
    header("Location: step3.php");
}


$tokenData = $_SESSION[PERMANENT_TOKENS_KEY];
$accessToken = json_decode($tokenData,true);

// connect to twitter oauth
$oauth = new TwitterOAuth(
    TWITTER_CONSUMER_KEY,
    TWITTER_CONSUMER_SECRET,
    $accessToken['oauth_token'],
    $accessToken['oauth_token_secret']
);

// verify credentials
$result = $oauth->get('account/verify_credentials');
if ($result->error)
{
    die("Something went wrong!");
}
else
{
    ?>
    <h1>Now we need your API key</h1>

    <p><img src="<?= $result->profile_image_url ?>" style="float:left; margin-right: 10px;">
    <p>Hi <?= $result->name ?>!
    
    <form method="post" action="step3.php">
    	<p>What's your awe.sm API key?
    	<p><input type="text" name="awesm_api_key" id="awesm_api_key"><input type="submit" value="Next">
    </form>    
    <?php
}

?>
<p>(If this isn't working properly, you can <a href="forget.php">start over</a>.)
