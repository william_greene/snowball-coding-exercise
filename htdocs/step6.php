<?php

require('config.php');
require('awesm.php');

session_start();
$apiKey = $_SESSION['awesm_api_key'];

$awesmId = $_REQUEST['id'];

$awesm = new Awesm($apiKey);
$url = $awesm->loadUrl($awesmId,array('domain' => 'demo.awe.sm'));

?>
<h1>About your URL...</h1>

<p>Awesm ID: <?= $url->getId(); ?>
<p>Original URL: <?= $url->getOriginalUrl(); ?>
<p>Clicks so far: <?= $url->getClicks(); ?>

<p>If you've not got any clicks, keep refreshing until you do.

<p>(API key used: <?= $apiKey ?>)

<p><a href="forget.php">Start over</a>
