<?php

require('config.php');
require('twitteroauth/twitteroauth.php');

session_start();
$tokenData = $_SESSION[PERMANENT_TOKENS_KEY];
$accessToken = json_decode($tokenData,true);

// connect to twitter oauth
$oauth = new TwitterOAuth(
    TWITTER_CONSUMER_KEY,
    TWITTER_CONSUMER_SECRET,
    $accessToken['oauth_token'],
    $accessToken['oauth_token_secret']
);
if (empty($oauth)) {
    die("Could not connect to Twitter");
}

$tweet = $_REQUEST['tweet'];
$awesmId = $_REQUEST['awesm_id'];
if (empty($tweet)) {
    die("Tweet not found");
}

$result = $oauth->post("http://api.twitter.com/1/statuses/update.json",array(
    'status' => stripslashes($tweet)
));

?>
<h1>Sent to Twitter!</h1>

<p>Tweet sent to Twitter. <a href="http://twitter.com/<?= $result->user->screen_name ?>/status/<?= $result->id ?>" target="_blank">See it on the site</a>.

<p><a href="step6.php?id=<?= $awesmId ?>">See the stats for this link</a>