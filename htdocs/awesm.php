<?php

class Awesm
{
    
    private $apiKey;
    
    const CREATE_URL = 'http://create.awe.sm/url.';
    const REDIRECT_URL = 'http://create.awe.sm/url/share';
    const FETCH_URL = 'http://create.awe.sm/url/';
    
    const CREATE = 1;
    const FETCH = 2;
    
    const DESTINATION_TWITTER = 'twitter';
    
    public function __construct($apiKey,$defaults=null)
    {
        $this->apiKey = $apiKey;
    }
    
    /* @TODO: setters for defaults that will apply to all links created
     * - format
     * - version
     * - share_type
     * - create_type
     * - parent_awesm
     * - domain
     * - sharer_id
     * - user_id
     * - notes
     * - callback
     */
    
    /**
     * Creates a new awesm URL from an original URL
     *
     * @param unknown_type $originalUrl
     * @param unknown_type $params
     * @return AwesmUrl
     */
    public function makeUrl($originalUrl,$params=null)
    {
        return new AwesmUrl($this->apiKey,Awesm::CREATE,$originalUrl,$params);
    }
    
    /**
     * Loads an existing URL
     *
     * @param unknown_type $awesmId
     * @return AwesmUrl
     */
    public function loadUrl($awesmId,$params=null)
    {
        return new AwesmUrl($this->apiKey,Awesm::FETCH,$awesmId,$params);
    }
    
}

class AwesmUrl
{
    /* creation properties */
    private $apiKey = null;
    private $originalUrl;
    private $version = '1';
    private $shareType = 'twitter';
    private $createType = 'api';
    private $parentAwesm = null;
    private $domain = 'awe.sm';
    private $sharerId = null;
    private $userId = null;
    private $notes = null;
    private $callback = null;
    
    const CALL_FORMAT = 'json';
    
    /* cached result of creation call */
    private $createResult = false;
    
    /* cached result of info call */
    private $infoResult = false;
    
    /* post-creation properties */
    private $id = false;
    private $shortUrl;
    private $redirectUrl;
    private $createdAt;
    
    /* stats properties */
    private $clicks;
    
    public function __construct($apiKey,$method,$urlOrId,$params=null)
    {
        $this->apiKey = $apiKey;
        if ($method == Awesm::CREATE)
        {
            $this->originalUrl = $urlOrId;
            // id not known; will be created
        }
        else
        {
            // id known; data will be fetch
            $this->id = $urlOrId;
        }
        if (!is_null($params))
        {
            foreach($params as $name => $value)
            {
                if(property_exists($this,$name))
                {
                    $this->$name = $value;
                }
            }
        }
    }
    
    /**
     * Creates a URL for the first time. Called only once per instantiation.
     * TODO: allow forcing of fresh creation calls?
     *
     * @return unknown
     */
    private function createUrl()
    {
        // if we already have an ID we won't do this
        if (!$this->id)
        {
            $createCall = Awesm::CREATE_URL . self::CALL_FORMAT;
    
            /* mapping internal properties to API parameters */
            $properties = array(
                'originalUrl' => 'target',
                'version' => 'version',
                'shareType' => 'share_type',
                'createType' => 'create_type',
                'apiKey' => 'api_key'
            );
            
            $params = array();
            foreach($properties as $propertyName => $paramName)
            {
                if(!is_null($this->$propertyName)) {
                    $params[$paramName] = $this->$propertyName;
                }
            }
            
            $ch = curl_init($createCall);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            $response = curl_exec($ch);
            
            error_log("CREATE call: $createCall\n with params " . print_r($params,true));
            $result = json_decode($response,true);
            if($result && isset($result['url']))
            {
                $u = $result['url'];
                $this->createResult = $u;
                // we overwrite even existing values so we can tell if calls went wrong
                $this->id =           $u['awesm_id'];
                $this->shortUrl =     $u['awesm_url'];
                $this->redirectUrl =  $u['redirect_url'];
                $this->originalUrl =  $u['original_url'];
                $this->shareType =    $u['share_type'];
                $this->createType =   $u['create_type'];
                $this->userId =       $u['user_id'];
                $this->notes =        $u['notes'];
                $this->parentAwesm =  $u['parent_awesm'];
                $this->createdAt =    $u['created_at'];
                $this->sharerId =     $u['sharer_id'];
                error_log("Result: " . print_r($this->createResult,true));
                return true;
            }
            else
            {
                error_log("Creation failed");
                return false;
            }
        }
        // we have an ID already, so wtf?
        throw new Exception("CreateURL called even though we have an awesm ID");
    }
    
    /**
     * Fetches info about an existing URL.
     * This call is repeatable.
     * TODO: cache optionally or by default?
     */
    private function fetchUrl()
    {
        // basic call
        $fetchCall = Awesm::FETCH_URL . $this->id . '.' . self::CALL_FORMAT;
        
        // parameters
        $fetchCall .= "?version=" . $this->version;
        $fetchCall .= "&domain=" . $this->domain;
        $fetchCall .= "&api_key=" . $this->apiKey;
        
        $ch = curl_init($fetchCall);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $response = curl_exec($ch);
        
        error_log("FETCH call: $fetchCall");
        error_log("Response: " . $response);
        $result = json_decode($response,true);
        error_log("Result: " . print_r($result,true));
        if ($result && $result['url'])
        {
            $u = $result['url'];
            $this->id =         $u['awesm_id'];
            $this->shortUrl =     $u['awesm_url'];
            $this->redirectUrl =  $u['redirect_url'];
            $this->originalUrl =  $u['original_url'];
            $this->shareType =    $u['share_type'];
            $this->createType =   $u['create_type'];
            $this->userId =       $u['user_id'];
            $this->notes =        $u['notes'];
            $this->parentAwesm =  $u['parent_awesm'];
            $this->createdAt =    $u['created_at'];
            $this->sharerId =     $u['sharer_id'];
            // fetch only
            $this->clicks =       $u['clicks'];
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Retrieves the requested property if it isn't populated already.
     * If this URL was created without an awesm_id, we use create.
     * If there is an awesm_id, then we use fetch.
     */
    private function getProperty($propertyName)
    {
        if(!isset($this->$propertyName))
        {
            error_log("property $propertyName not set");
            // do we have an awesm_id?
            if ($this->id)
            {
                error_log("awesm ID set; fetching");
                // great! Then we do a fetch call
                if ($this->fetchUrl())
                {
                    return $this->$propertyName;
                }
                else
                {
                    throw new Exception("Could not fetch URL info");
                }
            }
            else
            {
                error_log("No awesm ID; creating");
                // fine. We'll create it first
                // then we recurse, in case the property requested isn't a create property
                if ($this->createUrl())
                {
                    error_log("Created okay. Calling myself again for $propertyName");
                    return $this->getProperty($propertyName);    
                }
                else
                {
                    throw new Exception("Could not create URL");
                }
                // TODO: can this get into an infinite loop?
            }
        }
        else
        {
            error_log("$propertyName set; returning it");
            return $this->$propertyName;
        }
    }
    
    public function getId()
    {
        return $this->getProperty('id');
    }
    
    public function getShortUrl()
    {
        return $this->getProperty('shortUrl');
    }
    
    public function getOriginalUrl()
    {
        return $this->getProperty('originalUrl');
    }
    
    // FIXME: destination is being ignored...
    public function getRedirectUrl($destination=Awesm::DESTINATION_TWITTER)
    {
        return $this->getProperty('redirectUrl');
    }
    
    public function getClicks()
    {
        return $this->getProperty('clicks');
    }

    /**
     * Default toString action is the short URL
     * @return string
     */
    public function __toString()
    {
        return $this->getProperty('shortUrl');
    }
    
    /* stats methods start here */
    public function getClickStream($startDate,$endDate,$page = 1, $pageSize = 100)
    {
        
    }
    
}