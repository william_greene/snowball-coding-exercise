<?php

require('config.php');
require('twitteroauth/twitteroauth.php');

session_start();

if (empty($_SESSION['awesm_api_key']))
{
    if (empty($_REQUEST['awesm_api_key'])) {
        header("Location: step2.php");
        exit;
    }
    $_SESSION['awesm_api_key'] = $_REQUEST['awesm_api_key'];
}
$apiKey = $_SESSION['awesm_api_key'];

?>
<h1>Give us an RSS feed</h1>

<p>We tested using the feeds from del.icio.us, but most RSS should work.

<form method="post" action="step4.php">
	<p><input type="text" name="rss_feed_url" id="rss_feed_url"><input type="submit" value="shorten">
</form>

<p>The API key you submitted is <?= $apiKey ?>