<?php

require('config.php');
require('awesm.php');

session_start();
$apiKey = $_SESSION['awesm_api_key'];

$rssUrl = $_REQUEST['rss_feed_url'];
if (empty($rssUrl)) {
    die("No RSS URL");
}
$response = file_get_contents($rssUrl);
if (empty($response)) {
    die("No response fetching $rssUrl");
}
$rss = simplexml_load_string($response);
if (empty($rss)) {
    die("Could not parse RSS");
}

$items = $rss->channel->item;
$link = $items[0]->link;

$awesm = new Awesm($apiKey);
$shortUrl = $awesm->makeUrl($link,array('domain' => 'demo.awe.sm'));

?>
<h1>Link created!</h1>

<p>Your short URL is <?= $shortUrl; ?>. <a href="<?= $shortUrl ?>" target="_blank">Test it!</a>

<form method="post" action="step5.php">
<p><textarea name="tweet" cols="50"><?php
    echo "I'm testing out awe.sm. Click this link so I can get some stats! " . $shortUrl;
?></textarea>
<input type="hidden" name="awesm_id" value="<?= $shortUrl->getId(); ?>">
<p><input type="submit" value="Share on Twitter!">
</form>

<p>The API key used was <?= $_SESSION['awesm_api_key'] ?>