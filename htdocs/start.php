<h1>It's like Twitterfeed, but it's also a demo!</h1>

<p>Here's what's going down:
<ol>
	<li>You authorize us with Twitter</li>
	<li>You give us your awe.sm API key</li>
	<li>You give us an RSS feed</li>
	<li>We post the first link in that RSS feed to Twitter</li>
	<li>We give you stats of who's clicking on your link</li>
</ol>

<p>So let's get on with that!

<p><a href="step1.php">Start</a>

<hr>

<p>If this wasn't the demo, then a link to the source code of this app would be <a href="#usethesourceluke">here</a>. 