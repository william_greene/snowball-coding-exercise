<!doctype html>
<head>
	<title>awe.sm coding exercise</title>
	<!-- This code is deliberately terrible :-) Copying not recommended. -->
	<style>
		body {
			font-family: Arial, sans-serif;
			font-size: 13px;
		}
		#content {
			width: 40em;
			margin: auto;
		}
		.footnote {
			font-size: 88%;
			margin-top: 3em;
			padding-top: 0.5em;
			border-top: 1px solid #ccc;
		}
		li {
			margin-bottom: 0.5em;
		}
	</style>
</head>
<body>

<div id="content">
	<h1>awe.sm coding exercise</h1>
	
	<p>Hello awe.sm people! Hopefully you are here about the <a href="http://snowballfactory.jobscore.com/jobs/snowballfactory/frontendengineer/c7z0qYjber34_deJe4aGWH">front-end engineer</a> position we have open.
	
	<p>The thing about interviewing is that it's really time-consuming. You have to travel just to meet us, we both have to spend an hour talking, just to get a first impression -- and lots more than that to do it properly. There are several dozen of you, and we're a startup, so we don't have that kind of time. Plus, the question we really want to answer is: can you build stuff? And we figure the best way to prove that is by actually coding something.
	
	<p><a href="start.php">Start the app</a>
	
	<p>Step 2 will ask you for an API key. You can use this one:
	
	<p><b>6c8b1a212434c2153c2f2c2f2c165a36140add243bf6eae876345f8fd11045d3</b>
	
	<p>That's the demo API key, which works on the 'demo.awe.sm' domain (it's going to be important to remember that).
	

	<h2>Your task: build a functional copy of this app</h2>
	
	<p>By which we mean: a web app, installed and running on the Internet, that runs through a similar 
	multi-step process to share a URL pulled from an RSS feed on Twitter, using awe.sm to track the clicks. 
	The choice of development language, interaction design, and appearance is up to you (it doesn't need to 
	be on multiple pages if you don't want it to be). Once you're done, send an email with the URL of the 
	demo to <a href="mailto:laurie@snowballfactory.com">laurie@snowballfactory.com</a>.
	
	<p>Your version of the app should contain a link to a zip file containing the source code of your app.
	We read most languages, but if you're tempted to write it in Haskell please consider our 
	<a href="http://snowballfactory.jobscore.com/jobs/snowballfactory/backendengineer/cmIjpOjbqr34_deJe4aGWH">backend engineer</a> position. 
	
	<p>As you can see, this app is <i>extremely</i> simple. It's less than 300 lines of PHP<a href="#language">*</a>.
	It took us about 4 hours to build; we hope you won't need to spend much more than that on it.
	
	<h3>What we're looking for out of this is:</h3>
	<ul>
		<li>Semantic, standards-compliant HTML
		<li>High-quality, beautiful CSS
		<li>Attention to usability and accessibility
		<li>Robustness and error-checking
		<li>High-quality middle-layer code
		<li>The ability to read API documentation and turn that understanding into code
	</ul>
	
	<h3>What we are NOT looking for:</h3>
	<ul>
		<li>Additional features. The app is very simple in order to make it easy to write 
		and the code easy to review. We are fans of simplicity.</li>
		<li>Graphic design. If you are talented at Photoshop that counts in your favour, 
		but for the purposes of this exercise <b>use only CSS</b> for styling.</li>
		<li>Backwards compatibility. We will be testing in the latest versions of Chrome, 
		Firefox, Safari and IE8. If you would like to use newer CSS tricks that work in only one 
		or two of these, please indicate which browser we should use.</li>
		<li>Wheel reinvention. Please, <b>please</b> do not write a new OAuth library. 
		There are already perfectly good ones for Ruby, Python and PHP (see below).</li>
	</ul>
	
	<h2>Documentation and hints</h2>
	
	<p></p>

	http://twitter.rubyforge.org/
	http://code.google.com/p/oauth-python-twitter/
	http://github.com/abraham/twitteroauth
	
	http://groups.google.com/group/awesm-api/web/api-keys
	
	<p class="footnote"><a name="language">*</a> Most of our codebase is actually Ruby, but we were using this exercise as an excuse to write an awe.sm PHP library -- which we won't be releasing until after you're hired :-)
</div>

</body>
</html>