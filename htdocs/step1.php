<?php

require('config.php');
require('twitteroauth/twitteroauth.php');

session_start();
// are we current authorized?
if (empty($_SESSION[PERMANENT_TOKENS_KEY]))
{
    // no, we need to get a permanent token somehow
    
    // are they just coming back from authorizing?
    if (empty($_GET['oauth_token']))
    {
        // no, tell them where to go to authorize
        
        // connect to twitter oauth generically
        $oauth = new TwitterOAuth(
            TWITTER_CONSUMER_KEY,
            TWITTER_CONSUMER_SECRET
        );
        
        // get the temporary request token
        $requestToken = $oauth->getRequestToken();
        
        // store it for when they come back from authorizing
        $_SESSION[TEMP_TOKENS_KEY] = json_encode($requestToken);
        
        // send them off to get authorized
        $requestLink = $oauth->getAuthorizeURL($requestToken);

	?>        
	<h1>Give us permission to post to Twitter</h1>

        <p><a href="<?= $requestLink ?>">Authorize us with Twitter</a>
	<?php
        
    }
    else
    {
        // yes, get the new permanent token and store it
        
        // connect to twitter using the temp tokens 
        $tempToken = json_decode($_SESSION[TEMP_TOKENS_KEY],true);
        $oauth = new TwitterOAuth(
            TWITTER_CONSUMER_KEY,
            TWITTER_CONSUMER_SECRET,
            $tempToken['oauth_token'],
            $tempToken['oauth_token_secret']
        );

        // get the new permanent tokens
        $accessToken = $oauth->getAccessToken($_REQUEST['oauth_verifier']);
        
        // write the permanent tokens
        $_SESSION[PERMANENT_TOKENS_KEY] = json_encode($accessToken);
        
        // we should be good now
        header("Location: step2.php");
    }
}
else
{
    // yes, we've got tokens, so off to the next step 
    header("Location: step2.php");
}
